'use strict';
const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const Dotenv = require('dotenv-webpack');
module.exports = (env) => {
    // console.log('path',path.resolve);
    const MODE = env;
    const pathToEnvFile = '.' + MODE + '.env';
    const systemEnv = require('dotenv').config({path: pathToEnvFile}).parsed;
    return {
        entry: {
            'application': path.resolve(__dirname, process.env.frontendSourcePath) + '/application.js'
        },
        output: {
            filename: 'javascripts/[name].bundle.js',
            path: path.resolve(__dirname, process.env.frontendDestinationPath),
            publicPath: process.env.frontendProtocol +'://' + process.env.frontendHost + ':' + process.env.frontendPort +'/'
        },
        watch: MODE === 'dev',
        watchOptions: {
            aggregateTimeout: 300
        },
        plugins: [
            new Dotenv({
                path: pathToEnvFile,
                systemvars: true
            }),
            new HTMLWebpackPlugin({
                title: 'Test project',
                template: path.resolve(__dirname, process.env.frontendSourcePath) + '/index.html',
                baseUrl: process.env.frontendProtocol +'://' + process.env.frontendHost + ':' + process.env.frontendPort,
                faviconUrl: 'images/favicon.ico'
            }),
            new VueLoaderPlugin(),
            new OpenBrowserPlugin({
                url: process.env.frontendProtocol +'://' + process.env.frontendHost + ':' + process.env.frontendPort
            })
        ],
        module: {
            rules: [
                {
                    test: /\.vue$/,
                    loader: 'vue-loader',
                    options: {
                        loaders: {
                            js: 'babel-loader',
                            scss: 'vue-style-loader!css-loader!sass-loader',
                            sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax'
                        }
                    }
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.scss$/,
                    use: [
                        {
                            loader: 'style-loader'
                        },
                        {
                        loader: 'css-loader'
                    }, {
                        loader: 'resolve-url-loader'
                    }, {
                        loader: 'sass-loader'
                    }]
                },
                {
                    test: /\.(png|svg|jpg|gif|ico)$/,
                    loader: 'file-loader?name=images/[name].[ext]'
                },
                {
                    test: /\.(woff2?|eot|ttf|otf|svg)(\?\S*)?$/,
                    exclude: [/images/],
                    loader: 'file-loader?name=fonts/[name].[ext]'
                },
                {
                    test: /\.xml$/,
                    use: [
                        'xml-loader'
                    ]
                }
            ]
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.common.js',
            }
        }
    }
};
