export function trim(value) {
    return value.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
}

export function isTouchDevice() {
    return ('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch) || (navigator.maxTouchPoints > 0);
}

export function isMobile() {
    return typeof window.orientation !== 'undefined';
}

export function hasScroll(el) {
    if (!el) { return false; }
    return parseInt(el.scrollHeight) > parseInt(el.clientHeight);
}

export function isIOS() {
    return (!!navigator.userAgent.match(/(ipad|iphone|ipod)/gi) && !window.MSStream);
}

export function canPlayHls() {
    return !!document.createElement('video').canPlayType('application/vnd.apple.mpegURL');
}

export function base64Decode(value) {
    return Base64.decode(value);
}

export function base64Encode(value) {
    return Base64.encode(value);
}

export function getClosestParentByClass(domElement,cssClass) {
    return cssClasses.paByCls(domElement, cssClass);
}
export function getByClass(cssClass, place) {
    return cssClasses.allByCls(cssClass, place);
}
export function getFirstChildByClass(cssClass, place) {
    return cssClasses.firstByCls(cssClass, place);
}

export function findAncestor(el, cls) {
    while ((el = el.parentElement) && !el.classList.contains(cls));
    return el;
}

export function isParentNode(domElement, parentNode) {
    while(domElement = domElement.parentElement) {
        if(domElement == parentNode) {
            return true;
        }
    }
    return false;
}
function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top >= window.pageYOffset &&
        left >= window.pageXOffset &&
        (top + height) <= (window.pageYOffset + window.innerHeight) &&
        (left + width) <= (window.pageXOffset + window.innerWidth)
    );
}

export function fullScreenEl(el) {
    if (
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement
    ) {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    } else {
        let element = el || document.body;
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        } else {
            let videoTagElement = element.getElementsByTagName('video');
            if (videoTagElement && videoTagElement[0]) {
                let elV = videoTagElement[0];
                if (elV.webkitEnterFullScreen) {
                    elV.webkitEnterFullScreen();
                }
            }
        }
    }
}
let cssClasses = {
    firstByCls: function(cls, place) {
        place = (place) ? place : document;

        let els = place.getElementsByTagName('*');
        _.each(els, function(el) {
            if(cssClasses.has(el, cls)) {
                return el;
            }
        });
    },
    allByCls: function(cls, place) {
        let result = [];
        place = (place) ? place : document;
        let els = place.getElementsByTagName('*');
        _.each(els, function(el) {

            if(cssClasses.has(el, cls)) {
                result.push(el);
            }
        });
        return result;
    },
    paByCls: function(el, cls) {
        if( cssClasses.has(el, cls) ) {
            return el;
        }
        while(el = el.parentElement) {
            if(cssClasses.has(el, cls)) {
                return el;
            }
        }
    },
    has: function(el, cls) {
        let i;
        if (el.className === "") {
            return false;
        } else if (el.className === cls) {
            return true;
        } else {
            let classes = el.className.split(" ");
            for (i = 0; i < classes.length; i++) {
                if (classes[i] === cls) {
                    return true;
                }
            }
        }
    }

};
let Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    encode: function(e) {
        let t = "";
        let n, r, i, s, o, u, a;
        let f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    },
    decode: function(e) {
        let t = "";
        let n, r, i;
        let s, o, u, a;
        let f = 0;
        e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u !== 64) {
                t = t + String.fromCharCode(r)
            }
            if (a !== 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    },
    _utf8_encode: function(e) {
        e = e.replace(/\r\n/g, "\n");
        let t = "";
        for (let n = 0; n < e.length; n++) {
            let r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    },
    _utf8_decode: function(e) {
        let t = "";
        let n = 0;
        let r = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                let c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                let c2 = e.charCodeAt(n + 1);
                let c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t;
    }
};

export default {
    elementInViewport,
    cssClasses,
    fullScreenEl,
    hasScroll
}