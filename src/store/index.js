import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import route from './modules/route';
import domStates from './modules/domStates';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        route,
        domStates
    },
    strict: Vue.config.debug,
    plugins: Vue.config.debug ? [createLogger()] : []
});
