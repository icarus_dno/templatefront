export default {
    state: {
        routeCurrent: null
    },
    mutations: {
        setRouteCurrent(state, route) {
            state.routeCurrent = route;
        }
    },
    getters: {
        routeCurrent(state) {
            return state.routeCurrent;
        }
    }
};
