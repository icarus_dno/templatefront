export default {
    state: {
        isFullScreenMode: false
    },
    mutations: {
        setFullScreenMode(state, isFullscreen) {
            state.isFullScreenMode = isFullscreen;
        }
    },
    getters: {
        fullscreen(state) {
            return state.isFullScreenMode;
        }
    }
};
