import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    list(data) {
        return Api.get(`detail_ul?`, data);
    }
}
