import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    list(data, pageNumber) {
        if (pageNumber === undefined){
            pageNumber = 0;
        }
        return Api.get(`search?page=${pageNumber}&size=20`, data);
    },
    getCountries() {
        return Api.get(`list/country`);
    },
    getLocations() {
        return Api.get(`list/location`);
    },
    getStatuses() {
        return Api.get(`list/status`);
    },
    getALTypes() {
        return Api.get(`list/type`);
    },
    updateFilters(id, data) {
        return Api.get(`list_organizations`, data)
    },
    getFavoriteFilters() {
        return Api.get(`search/filters/by_type?type=search`);
    },
    addFavoriteFilter(data) {
        console.log("data", data);
        return Api.put(`search/filters`, data)
    },
    saveFavoriteFilter(data) {
        console.log("data", data);
        return Api.post(`search/filters`, {
            'id': data.id,
            'filterName': data.filterString
        })
    },
    deleteFavoriteFilter(data) {
        return Api.delete(`search/filters`,data);
    }
}
