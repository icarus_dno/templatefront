import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    newList(id){
        return Api.get(`dossier/AP?id=${id}`);
    },
    list(data) {
        return Api.get(`detail_al_os?`, data);
    },
    results(data) {
        return Api.get(`results_al_os?`, data);
    }
}
