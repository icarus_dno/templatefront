import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    list(data) {
        return Api.get(`detail_al_oei?`, data);
    },
    newList(id){
        return Api.get(`dossier/OEI?id=${id}`);
    },
    results(data) {
        return Api.get(`results_al_oei?`, data);
    }
}
