import Api from '../services/api';

export default {
    summaryInfo() {
        return Api.get(`panel/summary-info`);
    },
    countAl(date) {
        return Api.get(`panel/summary-info/count-al?date=${date}`);
    },
    dynamicAl(status) {
        return Api.get(`panel/summary-info/dynamic-al?status=${status}`);
    },
    stateService(date, type) {
        return Api.get(`panel/summary-info/state-service?date=${date}&type=${type}`); 
    },
    managerInfo(startDate,endDate, tuId) {
        return Api.get(`panel/manager-info?fromDate=${startDate}&toDate=${endDate}&tuId=${tuId}`);
    },
    monitoringInfo(startDate, endDate){
        return Api.get(`panel/monitoring?fromDate=${startDate}&toDate=${endDate}`);
    },
    alMapPi(fromDate, toDate, statusId, objectId){
        return Api.get(`panel/al-map/pi?fromDate=${fromDate}&toDate=${toDate}&statusId=${statusId}&objectId=${objectId}`);
    },
    alInfo(fromDate, toDate){
        return Api.get(`panel/al-info?fromDate=${fromDate}&toDate=${toDate}`);
    },
    alGraph(fromDate, toDate, statusId){
        return Api.get(`panel/al-info/al-graph?fromDate=${fromDate}&toDate=${toDate}&statusId=${statusId}`);
    },
    getStateControlInfo(fromDate, toDate, initiatorId, typeId){
        return Api.get(`panel/gos/info?fromDate=${fromDate}&toDate=${toDate}&initiatorId=${initiatorId}&typeId=${typeId}`);
    },
    getStateServicesInfo(fromDate, toDate, tuId){
        return Api.get(`panel/gu/info?fromDate=${fromDate}&toDate=${toDate}&tuId=${tuId}`);
    },
    getTu(){
        return Api.get(`panel/list/tu`);
    },
    getStatus(){
        return Api.get(`panel/list/status`);
    },
    getMapStatus(){
        return Api.get(`panel/list/status_map`);
    },
    getObject(){
        return Api.get(`panel/list/object`);
    },
    getTrEp(){
        return Api.get(`panel/list/trep`);
    },
    getCategory(trepId){
        return Api.get(`panel/list/category?treps=${trepId}`);
    },
    getTrNumber(trepId, categoryId){
        if (categoryId.length === 0){
            return Api.get(`panel/list/tr?treps=${trepId}`);
        } else {
            return Api.get(`panel/list/tr?treps=${trepId}&cats=${categoryId}`);
        }
    },
    getTopOS(fromDate, toDate, tuId, trepId, categoryId, trNum){
        if (categoryId.length === 0){
            return Api.get(`panel/al-info/top-os?fromDate=${fromDate}&toDate=${toDate}&tuId=${tuId}&trepId=${trepId}&trNum=${trNum}`);
        } else {
            return Api.get(`panel/al-info/top-os?fromDate=${fromDate}&toDate=${toDate}&tuId=${tuId}&trepId=${trepId}&category=${categoryId}&trNum=${trNum}`);
        }
    },
    getTopIL(fromDate, toDate, tuId, objectId){
        return Api.get(`panel/al-info/top-il?fromDate=${fromDate}&toDate=${toDate}&tuId=${tuId}&objectId=${objectId}`);
    },
    getQueryStatus(){
        return Api.get(`panel/list/query_status`);
    },
    getTypeAl(){
        return Api.get(`panel/list/type_al`);
    },
    getFGIS(){
        return Api.get(`panel/list/fgis`);
    },
    getUserGroup(){
        return Api.get(`panel/list/user_group`);
    },
    getDynamicAll(fromDate, toDate, userGroupId){
        return Api.get(`panel/monitoring/dynamic-all?fromDate=${fromDate}&toDate=${toDate}&userGroupId=${userGroupId}`);
    },
    getDynamicReg(fromDate, toDate, userGroupId){
        return Api.get(`panel/monitoring/dynamic-reg?fromDate=${fromDate}&toDate=${toDate}&userGroupId=${userGroupId}`);
    },
    getSMEVNumber(fromDate, toDate, statusId, isInto){
        return Api.get(`panel/monitoring/smev-total?fromDate=${fromDate}&toDate=${toDate}&statusId=${statusId}&isInto=${isInto}`);
    },
    getSMEV(fromDate, toDate, statusId, isInto){
        return Api.get(`panel/monitoring/smev-graph?fromDate=${fromDate}&toDate=${toDate}&statusId=${statusId}&isInto=${isInto}`);
    },
    getActivityOperations(fromDate, toDate, componentId, isOpen){
        return Api.get(`panel/monitoring/user-activity?fromDate=${fromDate}&toDate=${toDate}&componentId=${componentId}&isOpen=${isOpen}`);
    },
    getActivityUsers(fromDate, toDate, fgisId){
        return Api.get(`panel/monitoring/top-user?fromDate=${fromDate}&toDate=${toDate}&componentId=${fgisId}`);
    },
    getActivityAl(fromDate, toDate,alTypeIds){
        return Api.get(`panel/monitoring/top-al?fromDate=${fromDate}&toDate=${toDate}&typeId=${alTypeIds}`);
    },
    getActivityFgis(fromDate, toDate){
        return Api.get(`panel/monitoring/top-fgis?fromDate=${fromDate}&toDate=${toDate}`);
    },
    getExpertsInfo(fromDate, toDate){
        return Api.get(`panel/experts/info?fromDate=${fromDate}&toDate=${toDate}`);
    }




}