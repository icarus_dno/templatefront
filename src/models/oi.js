import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    list(data) {
        return Api.get(`detail_al_oi?`, data);
    },
    newList(id){
        return Api.get(`dossier/OI?id=${id}`);
    },
    results(data) {
        return Api.get(`results_al_oi?`, data);
    }
}
