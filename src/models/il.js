import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    newList(id){
        return Api.get(`dossier/RL?id=${id}`);
    },
    list(data) {
        return Api.get(`detail_al_il?`, data);
    },
    results(data) {
        return Api.get(`results_al_il?`, data);
    }
}
