import Api from '../services/api';

export default {
    getItemsPerPage() {
        return [100]
    },
    list() {
        return Api.get(`url`);
    },
    results(data) {
        return Api.get(`url`, data);
    }
}
