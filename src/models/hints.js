import Api from '../services/api';

export default {
    getHints(id) {
        return Api.get(`dossier/hints?id=${id}`);
    },
    getCategoryHints(){
        return Api.get(`dossier/tr_hints`);
    }
}
