import Api from '../services/api';

export default {
    getFavoriteFiltersByType(type){
        return Api.get(`search/filters/by_type?type=${type}`);
    },
    addFavoriteFilter(data) {
        return Api.put(`search/filters`, data);
    },
    deleteFavoriteFilter(id) {
        return Api.delete(`search/filters?id=${id}`,{});
    },
    saveFavoriteFilter(item){
        return Api.post(`search/filters`, {
            'id': item.id,
            'filterName': item.filterName
        })
    }
}