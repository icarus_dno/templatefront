import Vue from 'vue';

import Store from './store';
import Application from './components/application.vue';
import Api from './services/api';
import Router from './services/router';

Api.init();

let application = new Vue({
	el: '#application',
	template: '<application></application>',
	components: {
		application: Application
	},
	store: Store,
	router: Router.getInstance()
});

require('./assets/stylesheets/application.scss');
require('./assets/javascripts/assets.js');
