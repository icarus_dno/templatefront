import _ from 'lodash';

class Form {

    constructor(attributes = {}, scenario) {
        this.attributes = attributes;
        this.initialAttributes = _.clone(attributes);
        this.scenario = scenario;
        this.processing = false;
        this.errors = {};

        // populate errors for reactivity
        _.each(this.attributes, (value, key) => {
            this.errors[key] = null;
        });
    }

    setProcessing(state) {
        this.processing = state;
    }

    isProcessing() {
        return !!this.processing;
    }

    setScenario(scenario) {
        this.scenario = scenario;
    }

    getScenario() {
        return this.scenario;
    }

    isScenario(value) {
        return this.scenario === value;
    }

    setErrors(errors) {
        _.each(errors, (value, key) => {
            this.errors[key] = value;
        });
        this.errors = Object.assign({}, this.errors);
    }

    getError(error) {
        return this.errors[error];
    }

    hasError(error) {
        return !!this.errors[error];
    }

    clearErrors() {
        this.errors = {};
    }

    getAttributes() {
        return this.attributes;
    }

    setAttributes(attributes) {
        this.attributes = attributes;
    }

    setAttribute(attribute, value) {
        this.attributes[attribute] = value;
    }

    clearAttributes() {
        this.attributes = _.clone(this.initialAttributes);
    }

    makeFormData(files){
        let formData = new FormData();
        _.each(this.attributes, (value, key) => {
            if (value) {
                if (files[key]) {
                    formData.append(key, value, files[key])
                } else {
                    formData.append(key, value)
                }
            }
        });
        return formData;
    }

}

export default Form;
