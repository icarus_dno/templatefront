import Vue from 'vue';
import Axios from 'axios';
import Finally from 'promise.prototype.finally';

function init() {
    Finally.shim();
    Axios.defaults.baseURL = process.env.apiURL;
    Axios.defaults.headers.common.Accept = 'application/json';
}

function makeCall(method, url, data) {
    let request = {
        method: method,
        url: url,
        headers: {
            //'X-Auth-Token': User.getToken()
        }
    };
    if (method === 'GET' || method === 'DELETE') {
        request.params = data;
    } else {
        request.data = data;
    }
    return Axios(request).then((response) => {
        if (response.data) {
            return response.data;
        }
        return {code: 500, message: Vue.t('errors.unavailable_service')};
    });
}

export default {
    init,
    get: (url, data) => {
        return makeCall('GET', url, data);
    },
    post: (url, data) => {
        return makeCall('POST', url, data);
    },
    put: (url, data) => {
        return makeCall('PUT', url, data);
    },
    patch: (url, data) => {
        return makeCall('PATCH', url, data);
    },
    delete: (url, data) => {
        return makeCall('DELETE', url, data);
    }
}
