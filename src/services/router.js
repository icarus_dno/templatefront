import Vue from 'vue';
import VueRouter from 'vue-router';
import Bus from './bus';

import NotFound from '../components/not-found.vue';
import LayoutMain from '../components/layouts/main.vue';
import Main from '../components/main.vue';

const routes = [
    {
        path: '',
        component: LayoutMain,
        children: [
            {
                path: '',
                name: 'main',
                component: Main,
                meta: {
                    title: 'Главная'
                }
            },
            {
                path: 'not-found',
                name: 'not-found',
                component: NotFound,
                meta: {
                    title: 'Страницы с таким адресом не существует'
                }
            },
        ]
    },
    {
        path: '*',
        component: LayoutMain,
        children: [{
            path: '',
            component: NotFound
        }]
    }
];

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes,
    linkExactActiveClass: 'active'
});

router.beforeEach((to, from, next) => {
    next();
});

router.onError((error) => {

});

router.afterEach((to) => {
    let title = '';
    if (to.meta.title) {
        title = to.meta.title;
    }
    Bus.$emit('route:changed', to);
});

export default {
    getInstance() {
        return router;
    }
};
