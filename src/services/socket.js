import Socket from 'socket.io-client';

let socket = null;

function init() {
    socket = Socket.connect(window.config.bases.io);
}

function on() {
    socket.on.apply(socket, arguments);
}

function emit() {
    socket.emit.apply(socket, arguments);
}

function getInstance() {
    return socket;
}

export default {
    init,
    on,
    emit,
    getInstance
}
